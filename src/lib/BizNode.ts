import gk1 from '@/assets/svg/gk-1.svg'
import gk2 from '@/assets/svg/gk-2.svg'
import gk0 from '@/assets/svg/gk-0.svg'
export default {
    feedSwtich: {
        width: 100,
        height: 100,
        attrs: {
            body: {
                fill: '#f5f5f5',
                stroke: '#d9d9d9',
                strokeWidth: 1,
            },
        },
        data:{
            title:'低压馈电',
            type: 'feedSwtich',
        }
    },
    HighVoltageSwitch: {
        shape: 'image',
        width: 120,
        height: 80,
        imageUrl:
            gk2,
        // label: 'image',
        attrs: {},
        data: {
            title: '高压开关',
            type: 'HighVoltageSwitch',
            tag:'break',
            images:[gk0,gk1,gk2]
        }
    }
}