import { defineStore } from "pinia";
import { ref } from "vue";
import {Previewer} from "@/lib/Board"

export const useBoardDefaultStore = defineStore("boardDefault",()=>{

    const board = ref<Previewer|undefined>(undefined)

    return {board}
}) 