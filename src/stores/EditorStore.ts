import { Editor } from "@/lib/Board";
import { defineStore } from "pinia";
import { computed, reactive, ref } from "vue";

export const useEditorStore = defineStore("editor", () => {
    const editor = ref<Editor>(undefined)

    const graph = computed(() => {
        return editor.value.getGraph()
    })

    const jsonData = ref({})

    async function saveData() {
        jsonData.value = graph.value.toJSON()
        console.log(jsonData.value);
        
    }

    async function loadData() {
        graph.value.fromJSON(jsonData.value)
    }


    return { editor,graph,saveData,loadData,jsonData }
}) 