import { Cell, Graph } from "@antv/x6";
import { Dnd } from "@antv/x6-plugin-dnd";
import { Snapline } from "@antv/x6-plugin-snapline";
import { Transform } from "@antv/x6-plugin-transform";
import { Selection } from '@antv/x6-plugin-selection'

import { register } from '@antv/x6-vue-shape'
import RightMenu from "./RightMenu.vue";
import { h, render } from "vue";

import gk2 from "@/assets/svg/gk-2.svg"
import gk1 from "@/assets/svg/gk-1.svg"
import gk0 from "@/assets/svg/gk-0.svg"

export { initGraph, initPreview, loadPlugins, loadDnd, getNode }

function initGraph(containerDom: HTMLElement): Graph {
    const graph = init({
        container: containerDom,
        autoResize: true,
        background: {
            color: '#F2F7FA',
        },
        grid: {
            size: 10, // 网格大小 10px
            visible: true, // 绘制网格，默认绘制 dot 类型网格
        },
    })
    menucreate(graph)
    return graph
}

function initPreview(containerDom: HTMLElement) {
    const graph = init({
        container: containerDom,
        autoResize: true,
        background: {
            color: '#F2F7FA',
        },
        grid: {
            size: 10, // 网格大小 10px
            visible: true, // 绘制网格，默认绘制 dot 类型网格
        },
        interacting: false
    })



    return graph
}

function init(options: Graph.Options): Graph {
    const graph = new Graph(options)
    console.debug(new Date(), "  画板初始化完毕！");
    return graph
}

function loadPlugins(graph: Graph) {
    graph.use(
        new Snapline({
            enabled: true,
        }),
    )
    graph.use(
        new Transform({
            resizing: { enabled: true },
            rotating: { enabled: true },
        }),
    )
    graph.use(
        new Selection({
            enabled: true,
        }),
    )
}

function loadDnd(graph: Graph) {
    const dnd = new Dnd({
        target: graph,
    })
    return dnd
}

function getNode(graph: Graph, item: any) {
    if (!item) {
        return null
    }
    let node = null
    switch (item.shape) {
        case "rect":
            node = graph.createNode({
                shape: 'rect',
                width: 100,
                height: 40,
            })
            break;
        case "circle":
            node = graph.createNode({
                shape: 'circle',
                width: 80,
                height: 80,
            })
            break;
        case "gk":
            node = graph.createNode({
                shape: 'image',
                x: 290,
                y: 150,
                width: 60,
                height: 40,
                imageUrl:
                    // '@/assets/svg/gk-2.svg',
                    gk2,
                // label: 'image',
                attrs: {},
            })
            break;
        default:
            console.error("不支持的图形类型！")
            break;
    }
    return node
}

/**
 * 添加右键菜单
 */
function menucreate(graph: Graph) {
    register({
        shape: 'right-menu-node',
        width: 200,
        height: 100,
        component: RightMenu,
    })
    let son = document.createElement('div')
    let container = document.getElementById("board-layout")
    container.insertBefore(son, container.firstChild)
    graph.on('node:contextmenu', ({ e, x, y, node, view }) => {
        son.style.position = 'absolute'
        son.style.zIndex = '9999'
        son.style.left = e.clientX + 'px'
        son.style.top = e.clientY + 'px'
        let vnode = h(RightMenu, { node, graph, x, y })
        render(vnode, son)
    })
    graph.on("blank:click", ({ e }) => {
        render(null, son)
    })
    window._destroy = function () {
        render(null, son)
    }

    // 监听 keydown 事件
    window.addEventListener('keydown', function (event) {
        let cell: Cell = graph.getSelectedCells()[0]
        // 检查按下的键是否为 Delete 键
        if (event.key === 'Delete') {
            // 按下的是 Delete 键，执行相应的操作
            console.log('Delete key was pressed');

            if (!cell) {
                return
            }
            // 可以添加其他处理逻辑
            console.log(cell);
            graph.removeCell(cell)

        }
        if (event.key === 's') {
            console.log(graph.getNodes());
            if (!cell) {
                return
            }
            console.log("修改", cell);
            if (cell.shape == "image") {
                if (cell.attr("image/xlink:href") == gk2)
                    cell.attr("image/xlink:href", gk1)
                else
                    cell.attr("image/xlink:href", gk2)

            }
        }
    });

}
/**
 * 添加右键菜单
 */
function menuClick(graph: Graph) {
    graph.on('node:contextmenu', ({ e, x, y, node, view }) => {

    })
}